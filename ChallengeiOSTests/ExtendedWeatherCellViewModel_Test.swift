//
//  ExtendedWeatherCellViewModel_Test.swift
//  ChallengeiOSTests
//
//  Created by Analia Rossi on 18/01/2021.
//

import XCTest
@testable import ChallengeiOS

class ExtendedWeatherCellViewModel_Test: XCTestCase {

    var subject: ExtendedWeatherCellViewModel!
    
    let dailyInfo = Daily(dt: 1611086400, sunrise: 1611069565, sunset: 1611105490, temp: Temp(day: 15.67, min: 10.59, max: 16.52, night: 10.9, eve: 13.46, morn: 11.1), feelsLike: FeelsLike(day: 9.09, night: 7.98, eve: 9.52, morn: 4.33), pressure: 1014, humidity: 29, dewPoint: -7.09, windSpeed: 6.11, windDeg: 23, weather: [Weather(description: "cielo claro", icon: "01d", main: "Clear")], clouds: 0, pop: 0, uvi: 2/36)

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        subject = ExtendedWeatherCellViewModel(withDailyInfo: dailyInfo)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        subject = nil
    }

    func test_getName() {
        // Given
        // When
        let result = subject.getDate()
        // Then
        XCTAssertEqual(result, "Tue, Jan 19")
    }
    
    func test_getTemp() {
        // Given
        // When
        let result = subject.getTemp()
        // Then
        XCTAssertEqual(result, "11.0/17.0°C")
    }

    func test_getIconUrl() {
        // Given
        // When
        let result = subject.getIconUrl()
        // Then
        XCTAssertEqual(result, "https://openweathermap.org/img/w/01d.png")
    }
}
