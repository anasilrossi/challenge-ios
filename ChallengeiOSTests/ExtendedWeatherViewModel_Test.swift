//
//  ChallengeiOSTests.swift
//  ChallengeiOSTests
//
//  Created by Analia Rossi on 13/01/2021.
//

import XCTest
@testable import ChallengeiOS

class ExtendedWeatherViewModel_Test: XCTestCase {
    
    var subject: ExtendedWeatherViewModel?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        if let cityWeather = obtainCity() {
            subject = ExtendedWeatherViewModel(cityWeather: cityWeather)
            subject?.apiClient = MockApiClientSuccess()
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        subject = nil
    }
    
    private func obtainCity() -> CityWeather? {
        guard let data = readJson(forResource: "cityWeather") else {
            return nil
        }
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .secondsSince1970
            let result = try decoder.decode(CityWeather.self, from: data)
            return result
        } catch {
            return nil
        }
    }

    func test_fetch_success() {
        // Given
        // When
        subject?.fetch()
        // Then
        XCTAssertEqual(subject?.getDaily(atIndex: 0).dt, 1611000000)
        XCTAssertEqual(subject?.getDaily(atIndex: 0).temp.max, 20.99)
        XCTAssertEqual(subject?.getDaily(atIndex: 1).dt, 1611086400)
        XCTAssertEqual(subject?.getDaily(atIndex: 1).temp.max, 16.52)

    }
    
    func test_fetch_failure() {
        // Given
        var error: String?
        subject?.apiClient = MockApiClientFailure()
        subject?.errorHandler = { string in
            error = string
        }
        // When
        subject?.fetch()
        // Then
        XCTAssertNotNil(error)
        XCTAssertEqual(error, "ERROR")
    }
    
    func test_getNumberOfRows() {
        // Given
        subject?.fetch()
        // When
        let result = subject?.getNumberOfRows()
        // Then
        XCTAssertEqual(result, 8)
    }
    
    func test_getCell_AtIndexPath() {
        // Given
        subject?.fetch()
        let mockTableView = UITableView()
        mockTableView.register(cell: ExtendedWeatherTableViewCell.self)
        // When
        let result = subject?.getCell(fromTableView: mockTableView, atIndexPath: IndexPath(row: 1, section: 0))
        // Then
        XCTAssertTrue(result is ExtendedWeatherTableViewCell)
        let castedResult = result as! ExtendedWeatherTableViewCell
        XCTAssertNotNil(castedResult.viewModel)
    }
    
    private func readJson(forResource fileName: String ) -> Data? {
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                return data

            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
        
        return nil
    }
}

