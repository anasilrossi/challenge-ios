//
//  MockApiClient.swift
//  ChallengeiOSTests
//
//  Created by Analia Rossi on 18/01/2021.
//

import Foundation
@testable import ChallengeiOS


class MockApiClientSuccess: WeatherAPIProtocol {
    func findBy(cityName: String, completionHandler: @escaping ApiCompletionHandler<SearchResult>) {
        
    }
    
    func fetchBy(cityID: [Int], completionHandler: @escaping ApiCompletionHandler<SearchResult>) {
        
    }
    
    func fetchBy(lat: Double, long: Double, completionHandler: @escaping ApiCompletionHandler<CityWeather>) {
        
    }
    
    func fetchExtendedWeather(lat: Double, lon: Double, completionHandler: @escaping ApiCompletionHandler<[Daily]>) {
        guard let data = readJson(forResource: "extendedWheater") else {
            completionHandler(.error("JSON ERROR"))
            return
        }
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .secondsSince1970
            let result = try decoder.decode(CityExtendedWeather.self, from: data)
            completionHandler(.success(result.daily))
        } catch(let error) {
            completionHandler(.error(error.localizedDescription))
        }
        
    }
    
    private func readJson(forResource fileName: String ) -> Data? {
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                return data

            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
        
        return nil
    }
}

class MockApiClientFailure: WeatherAPIProtocol {
    func findBy(cityName: String, completionHandler: @escaping ApiCompletionHandler<SearchResult>) {
        completionHandler(.error("ERROR"))

    }
    
    func fetchBy(cityID: [Int], completionHandler: @escaping ApiCompletionHandler<SearchResult>) {
        completionHandler(.error("ERROR"))

    }
    
    func fetchBy(lat: Double, long: Double, completionHandler: @escaping ApiCompletionHandler<CityWeather>) {
        completionHandler(.error("ERROR"))

    }
    

    func fetchExtendedWeather(lat: Double, lon: Double, completionHandler: @escaping ApiCompletionHandler<[Daily]>) {
        completionHandler(.error("ERROR"))
    }
    
}




