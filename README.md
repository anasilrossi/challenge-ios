# Challenge iOS

Challenge iOS para entrevista con Telecom - Flow

Se desarrollo una applicacion de consulta del clima, donde se puede visualizar el clima actual de la ubicacion actual del usuario y el usuario puede elegir otras ciudades para agregar a la lista. Ademas al hacer tap en alguna de ellas el usuario podra ver el pronostico extendido para los proximos dias. 

La arquitectura que elegi para el desarrollo de la applicacion es : **MVVVM** (Model View View Model)

Para la capa de networking utilice **URLSession** para las llamadas a las API, pero tambien se podria haber echo con Alamofire como framework externo.
La API  para obtener la data fue **​​Open Weather Map** y los endpoints utilizados fueron los siguientes: 

    - findByCityName: api.openweathermap.org/data/2.5/find?q=London&units=metric
    - fetchByCityIDs: api.openweathermap.org/data/2.5/group?id=524901,703448,2643743
    - fetchBylatitudlongitud: api.openweathermap.org/data/2.5/weather?lat=35&lon=139
    - fetchExtendedWeatherlatitudlongitud: api.openweathermap.org/data/2.5/onecall?lat=35&lon=139

Estos mismos son los metodos declarados en **WeatherAPIProtocol**

Para obtener la ubicacion del usuario utilice **CoreLocation** en la clase **LocationManager**

Para persistir la ciudades elegidas por el usuario utilice **NSUserDefault** ya que lo unico que necesita para identificar las ciudades y poder obtener el clima actulizado una vez que el usuario vuelva a entrar a la aplicacion era el ID de cada ciudad. En el caso que hubiera necesitad guardad mas informacion de las ciudades, me hubiera inclinado por usar **CoreData** o **Realm** como persister. 

Para la UI decidi utilizar Storyboard para los ViewControllers y xib para las UITableViewCell

Se agregaron las siguientes extensiones:
- **UIImageView**: Para descargar imagenes desde una URL y cachearlas.
- **Thread**: Uso mas facil y mas legibles del Thread.
- **UITableView**: Metodo para registrar las celdas
- **URL**: Metodo para agregar queryItems a una URL
- **UIView**: Metodo para simplificar la creacion de NIB
- **String**: Metodo para pasar timeStamp y  devolver un String con un formato especifico.

Se realizo los Unit test de 2 clases:
- ExtendedWeatherViewModel
- ExtendedWeatherCellViewModel



