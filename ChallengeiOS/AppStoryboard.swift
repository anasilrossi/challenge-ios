//
//  AppStoryboard.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 13/01/2021.
//

import Foundation
import UIKit

enum AppStoryboard: String {
    case main = "Main"
    
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: .main)
    }
    
    func viewController<T: UIViewController>(withType type: T.Type) -> T {
        let storyboardId = type.storyboardId
        
        guard let vc = instance.instantiateViewController(withIdentifier: storyboardId) as? T else {
            fatalError()
        }
        
        return vc
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController {
    class var storyboardId: String {
        return "\(self)"
    }
    
    static func instantiate(from storyboard: AppStoryboard) -> Self {
        return storyboard.viewController(withType: self)
    }
}
