//
//  UITableView+.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 14/01/2021.
//

import UIKit

extension UITableView {
    /// Registers a specific cell class to the table view
    /// using the class identifier to create a nib instance
    /// - parameters:
    ///     - cell: The representative UITableViewCell class or subclass
    func register<T>(cell: T.Type) where T: UITableViewCell {
        self.register(UINib(nibName: T.identifier(), bundle: nil), forCellReuseIdentifier: T.identifier())
    }
}
