//
//  Thread+.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 14/01/2021.
//

import Foundation

extension Thread {
    /// Execute code on the main thread, asynchronously
    /// - Parameters:
    ///     - completion: The closure to be executed
    static func runOnMainQueue(completion: @escaping () -> Void) {
        DispatchQueue.main.async {
            completion()
        }
    }
    
    /// Execute code on a Background thread, asynchronously
    /// - Parameters:
    ///     - completion: The closure to be executed
    static func runOnBackgroundQueue(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .background).async {
            completion()
        }
    }
    
    /// Execute code on a User Initiated thread, asynchronously
    /// - Parameters:
    ///     - completion: The closure to be executed
    static func runOnUserInitiatedQueue(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            completion()
        }
    }
    
    /// Execute code on a User Interactive thread, asynchronously
    /// - Parameters:
    ///     - completion: The closure to be executed
    static func runOnUserInteractiveQueue(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .userInteractive).async {
            completion()
        }
    }
    
    /// Execute code on a Utility thread, asynchronously
    /// - Parameters:
    ///     - completion: The closure to be executed
    static func runOnUtilityQueue(completion: @escaping () -> Void) {
        DispatchQueue.global(qos: .utility).async {
            completion()
        }
    }
}
