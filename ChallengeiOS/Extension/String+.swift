//
//  String+.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 18/01/2021.
//

import Foundation

extension String {
    public static func getDateFromTimeStamp(timeStamp : Double, dateFormat: String) -> String {
        /// Convert `timeStamp` to NSDate
        let date = NSDate(timeIntervalSince1970: timeStamp)
        let dayTimePeriodFormatter = DateFormatter()
        /// Use dateFormat according to `dateFormat` paramerter
        dayTimePeriodFormatter.dateFormat = dateFormat
        /// Convert Date to String
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
}
