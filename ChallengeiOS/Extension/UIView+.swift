//
//  UIView+.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 14/01/2021.
//

import Foundation
import UIKit

// MARK: - UINib Instance
extension UIView {
    /// Returns an instance of the given type instantiated from a nib
    /// with the same name as the class
    class func createFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(T.identifier(), owner: nil, options: nil)![0] as! T
    }
    
    /// Returns the class unique name as an identifier
    public class func identifier() -> String {
        return String(describing: self.classForCoder())
    }
}
