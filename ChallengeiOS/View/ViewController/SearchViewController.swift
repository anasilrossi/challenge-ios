//
//  SearchViewController.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 15/01/2021.
//

import UIKit

class SearchViewController: UIViewController {
    //MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    // MARK: - ViewModel
    let viewModel: SearchViewModel = SearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        setupTableView()
        setupSearchBar()
    }
    
    
    // MARK: PRIVATE METHOD
    private func setupViewModel() {
        // Setup the UI Update Handler
        viewModel.searchListBinding = { [weak self] in
            guard let strongSelf = self else { return }
            Thread.runOnMainQueue {
                // Reload table
                strongSelf.tableView.reloadData()
            }
        }
        // Setup the Error Handler
        viewModel.errorHandler = { [weak self] error in
            guard let strongSelf = self else { return }
            Thread.runOnMainQueue {
                let alert: UIAlertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                strongSelf.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func setupTableView() {
        tableView.register(cell: WeatherTableViewCell.self)
        tableView.alwaysBounceVertical = false
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    private func setupSearchBar() {
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.becomeFirstResponder()
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text {
            viewModel.findBy(textValue: searchText)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            viewModel.cleanList()
        }
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCell(fromTableView: tableView, atIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


