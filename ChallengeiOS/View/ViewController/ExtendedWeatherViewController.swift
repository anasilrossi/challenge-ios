//
//  ExtendedWeatherViewController.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 16/01/2021.
//

import UIKit

class ExtendedWeatherViewController: UIViewController {

    //MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - ViewModel
    var viewModel: ExtendedWeatherViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        setupTableView()
    }
    
    //MARK: - ACTION METHOD
    
    @objc func handleRefresh() {
        viewModel.fetch()
    }

    //MARK: PRIVATE METHOD
    
    private func setupViewModel() {
        viewModel.fetch()
        // Setup the UI Update Handler
        viewModel.extendedListBinding = { [weak self] in
            guard let strongSelf = self else { return }
            Thread.runOnMainQueue {
                // Reload table
                strongSelf.tableView.reloadData()
                // Stop the refresh in case needed
                strongSelf.tableView.refreshControl?.endRefreshing()
            }
        }
        // Setup the Error Handler
        viewModel.errorHandler = { [weak self] error in
            guard let strongSelf = self else { return }
            Thread.runOnMainQueue {
                let alert: UIAlertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                strongSelf.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func setupTableView() {
        tableView.register(cell: ExtendedWeatherTableViewCell.self)
        tableView.alwaysBounceVertical = false
        tableView.dataSource = self
        tableView.delegate = self
        // Add the refresh control to the tableview
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        tableView.refreshControl = refreshControl
    }
}

extension ExtendedWeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCell(fromTableView: tableView, atIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
