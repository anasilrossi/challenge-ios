//
//  ListViewController.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 13/01/2021.
//

import UIKit

enum TableViewSection: Int, CaseIterable {
    case currentLocation = 0
    case bookmark
}

class ListViewController: UIViewController {
    //MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - ViewModel
    let viewModel: ListViewModel = ListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupViewModel()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.fetch()
    }
    // MARK: - PUBLIC METHOD
    
    @objc func handleRefresh() {
        viewModel.fetch()
    }
    
    // MARK: - PRIVATE METHOD
    
    private func setupNavigationBar() {
        navigationController?.isNavigationBarHidden = false
        let searchImage: UIImage = UIImage(systemName: "magnifyingglass")!
        let searchMenu = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(tapSearch))
        searchMenu.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItems = [searchMenu]
        
        navigationItem.title = "Weather App"
    }
    
    private func setupViewModel() {
        // Setup the UI Update Handler
        viewModel.cityManager.weatherListBinding = { [weak self] in
            guard let strongSelf = self else { return }
            Thread.runOnMainQueue {
                // Reload table
                strongSelf.tableView.reloadData()
                // Stop the refresh in case needed
                strongSelf.tableView.refreshControl?.endRefreshing()
                
            }
        }
        // Setup the Error Handler
        viewModel.cityManager.errorHandler = { [weak self] error in
            guard let strongSelf = self else { return }
            Thread.runOnMainQueue {
                // Show Alert
                let alert: UIAlertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                strongSelf.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    private func setupTableView() {
        tableView.register(cell: WeatherTableViewCell.self)
        tableView.alwaysBounceVertical = false
        tableView.dataSource = self
        tableView.delegate = self
        // Add the refresh control to the tableview
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        tableView.refreshControl = refreshControl
    }
    
    private func showDetails(atIndex index: IndexPath) {
        let detailsViewController = ExtendedWeatherViewController.instantiate(from: .main)
        guard let cityWeather = viewModel.getCityWeatherInfo(index: index) else { return }
        detailsViewController.viewModel = ExtendedWeatherViewModel(cityWeather: cityWeather)
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    // MARK: - ACTION METHOD
    @objc func tapSearch() {
        let searchViewController = SearchViewController.instantiate(from: .main)
        self.present(searchViewController, animated: true, completion: nil)
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableViewSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.getTitleForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCell(fromTableView: tableView, atIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showDetails(atIndex: indexPath)
    }
}


