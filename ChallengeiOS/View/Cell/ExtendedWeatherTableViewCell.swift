//
//  ExtendedWeatherTableViewCell.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 16/01/2021.
//

import UIKit

class ExtendedWeatherTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherIconImage: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    
    //MARK: - ViewModel
    var viewModel: ExtendedWeatherCellViewModel! {
        didSet {
           self.updateCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        weatherIconImage.image = nil
    }
    
    //MARK: - PRIVATE METHOD
    private func updateCell() {
        self.dateLabel.text = viewModel.getDate()
        self.weatherIconImage.download(from: viewModel.getIconUrl())
        self.tempLabel.text = viewModel.getTemp()
    }
}
