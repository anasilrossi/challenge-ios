//
//  WeatherTableViewCell.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 14/01/2021.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    //MARK: - IBOUTLET
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityIcon: UIImageView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bookmarkButton: UIButton!
    
    //MARK: - ViewModel
    var viewModel: WeatherCellViewModel? {
        didSet {
           self.updateCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 10
        // Set selection style to .none to avoid visual issues with the AsyncImageView
        // The selection animation behaves on the layer contents, resulting on a undesired
        // behavior when scrolling and selecting
        self.selectionStyle = .none

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        // Clean image
        iconImage.image = nil
        cityIcon.image = nil
    }
    
    // MARK: - PRIVATE METHOD
    private func updateCell() {
        guard let viewModel = self.viewModel else { return }
        self.dateLabel.text = viewModel.getDate()
        self.cityLabel.text = viewModel.getCityName()
        self.tempLabel.text = viewModel.getTemp()
        iconImage.download(from: viewModel.getIconUrl(), contentMode: .scaleAspectFill)
        cityIcon.download(from: viewModel.getFlagUrl(), contentMode: .scaleAspectFill)
        self.descriptionLabel.text = viewModel.getDescription()
        self.locationIcon.isHidden = !viewModel.isCurrentLocation
        boomkarkButtuonUI()
    }
    
    private func boomkarkButtuonUI() {
        guard let viewModel = self.viewModel else { return }
        if viewModel.isCurrentLocation {
            self.bookmarkButton.isHidden = true
        } else {
            self.bookmarkButton.isHidden = false
            if viewModel.isInWeatherList() {
                self.bookmarkButton.setImage(UIImage(systemName: "bookmark.fill"), for: .normal)
            } else {
                self.bookmarkButton.setImage(UIImage(systemName: "bookmark"), for: .normal)
            }
        }
        
    }

    // MARK: - ACTION METHOD
    @IBAction func didTapBookmarkButton(_ sender: Any) {
        guard let viewModel = self.viewModel else { return }
        viewModel.didTapBookmarkButton()
        boomkarkButtuonUI()

    }
}

