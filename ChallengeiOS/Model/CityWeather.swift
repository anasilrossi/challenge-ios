//
//  CityWeather.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 13/01/2021.
//

import Foundation

// MARK: - SearchResult
public struct SearchResult: Decodable {
    let list: [CityWeather]
}

// MARK: - CityWeather
public struct CityWeather: Decodable, Equatable {
    let id: Int
    let main: Main
    let name: String
    let coord: Coord
    let sys: Sys
    let weather: [Weather]
    let wind: Wind
    let dt: Double
    var isCurrentLocation: Bool
    
    private enum CodingKeys: String, CodingKey { case id, main, name, coord, sys, weather, wind, dt }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        main = try container.decode(Main.self, forKey: .main)
        name = try container.decode(String.self, forKey: .name)
        coord = try container.decode(Coord.self, forKey: .coord)
        sys = try container.decode(Sys.self, forKey: .sys)
        weather = try container.decode([Weather].self, forKey: .weather)
        wind = try container.decode(Wind.self, forKey: .wind)
        dt = try container.decode(Double.self, forKey: .dt)
        isCurrentLocation = false
    }
    
    public static func == (lhs: CityWeather, rhs: CityWeather) -> Bool {
        return lhs.id == rhs.id
    }
}

// MARK: - Main
struct Main: Decodable {
    let humidity: Int
    let temp: Double
    let tempMin, tempMax: Double
}

// MARK: - Coord
struct Coord: Decodable {
    let lat: Double
    let lon: Double
}

// MARK: - Sys
struct Sys: Decodable {
    let country: String
}

// MARK: - Weather
struct Weather: Decodable {
    let description: String
    let icon: String
    let main: String
}

// MARK: - Wind
struct Wind: Codable {
    let speed: Double
    let deg: Int
}
