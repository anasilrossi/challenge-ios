//
//  CityExtendedWeather.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 16/01/2021.
//

import Foundation

// MARK: - CityExtendedWeather
struct CityExtendedWeather: Decodable {
    let lat: Double
    let lon: Double
    let daily: [Daily]
}

// MARK: - Daily
struct Daily: Decodable {
    let dt, sunrise, sunset: Double
    let temp: Temp
    let feelsLike: FeelsLike
    let pressure, humidity: Int
    let dewPoint, windSpeed: Double
    let windDeg: Int
    let weather: [Weather]
    let clouds: Int
    let pop, uvi: Double
}

// MARK: - FeelsLike
struct FeelsLike: Decodable {
    let day, night, eve, morn: Double
}

// MARK: - Temp
struct Temp: Decodable {
    let day, min, max, night: Double
    let eve, morn: Double
}
