//
//  AppDelegate.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 13/01/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
//        navigationController.viewControllers = [rootViewController]
//        self.window?.rootViewController = navigationController
//        self.window?.makeKeyAndVisible()
        applyStyling()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Private
    private func applyStyling() {
        let navBarProxy = UINavigationBar.appearance()
        navBarProxy.barTintColor = UIColor(red: 102.0/255.0, green: 0, blue: 0, alpha: 0.6)
        navBarProxy.tintColor = UIColor.white
        navBarProxy.titleTextAttributes = [.foregroundColor: UIColor.white]
        navBarProxy.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    }
}

