//
//  WeatherAPIManager.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 13/01/2021.
//

import Foundation

public enum ApiResult <T> {
    case success(T)
    case error(String)
}

public enum NetworkResponse: String {
    case success
    case authenticationError    = "You need to be authenticated."
    case badRequest             = "400 !! Bad request."
    case outdated               = "The url you requested is outdated."
    case failed                 = "Network request failed."
    case noData                 = "Response returned with no data to decode."
    case unableToDecode         = "We could not decode the response."
}

typealias ApiCompletionHandler<T> = ((ApiResult<T>) -> Void)

protocol WeatherAPIProtocol {
    /// Functions to find the weather information for CityName
    /// - parameters:
    ///     - cityName: Name of the city that need to search
    ///     - completionHandler: A closure that executes sending the  results as params
    func findBy(cityName: String, completionHandler: @escaping ApiCompletionHandler<SearchResult>)
    
    /// Functions to find the weather information for cityID
    /// - parameters:
    ///     - cityID: Id of the citys that need to search
    ///     - completionHandler: A closure that executes sending the  results as params
    func fetchBy(cityID: [Int], completionHandler: @escaping ApiCompletionHandler<SearchResult>)
    
    /// Functions to find the weather information for latitud and longitud
    /// - parameters:
    ///     - lat: Latitud of the city
    ///     - long: Longitud of the city
    ///     - completionHandler: A closure that executes sending the  results as params
    func fetchBy(lat: Double, long: Double, completionHandler: @escaping ApiCompletionHandler<CityWeather>)
    
    /// Functions to find the extended weather information for latitud and longitud
    /// - parameters:
    ///     - lat: Latitud of the city
    ///     - long: Longitud of the city
    ///     - completionHandler: A closure that executes sending the  results as params
    func fetchExtendedWeather(lat: Double, lon: Double, completionHandler: @escaping ApiCompletionHandler<[Daily]>)
}

public class WeatherAPIManager: WeatherAPIProtocol {
    
    let baseURL: URL = URL(string: "https://api.openweathermap.org/data/2.5/")!
    let appikey: String = "9315a36c5ac91522f3bda4cbd189e73a"
    
    // MARK: - PUBLIC
    func findBy(cityName: String, completionHandler: @escaping ApiCompletionHandler<SearchResult>) {
        let url = baseURL.appendingPathComponent("find")
        let finalURL = url.appending("q", value: cityName)
            .appending("appid", value: appikey)
            .appending("units", value: "metric")
            .appending("lang", value: "es")
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = "GET"
        
        // Define URL for Weather Data
        URLSession.shared.dataTask(with: finalURL) { data, response, error in
            guard error == nil else {
                return completionHandler(.error(NetworkResponse.failed.rawValue))
            }
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        return completionHandler(.error(NetworkResponse.noData.rawValue))
                    }
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        decoder.dateDecodingStrategy = .secondsSince1970
                        let result = try decoder.decode(SearchResult.self, from: responseData)
                        completionHandler(.success(result))
                    } catch {
                        completionHandler(.error(NetworkResponse.unableToDecode.rawValue))
                    }
                    
                case .error(let networkFailureError):
                    completionHandler(.error(networkFailureError))
                }
            }
       }.resume()
    }
    
    func fetchBy(cityID: [Int], completionHandler: @escaping ApiCompletionHandler<SearchResult>) {
        let url = baseURL.appendingPathComponent("group")
        let cityIDString = (cityID.map{String($0)}).joined(separator: ",")
        let finalURL = url.appending("id", value: cityIDString)
            .appending("appid", value: appikey)
            .appending("units", value: "metric")
            .appending("lang", value: "es")
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = "GET"
        
        // Define URL for Weather Data
        URLSession.shared.dataTask(with: finalURL) { data, response, error in
            guard error == nil else {
                return completionHandler(.error(NetworkResponse.failed.rawValue))
            }
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        return completionHandler(.error(NetworkResponse.noData.rawValue))
                    }
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        decoder.dateDecodingStrategy = .secondsSince1970
                        let result = try decoder.decode(SearchResult.self, from: responseData)
                        completionHandler(.success(result))
                    } catch {
                        completionHandler(.error(NetworkResponse.unableToDecode.rawValue))
                    }
                    
                case .error(let networkFailureError):
                    completionHandler(.error(networkFailureError))
                }
            }
       }.resume()
    }
    
    func fetchBy(lat: Double, long: Double, completionHandler: @escaping ApiCompletionHandler<CityWeather>) {
        let url = baseURL.appendingPathComponent("weather")
        let finalURL = url.appending("lat", value: "\(lat)")
            .appending("lon", value: "\(long)")
            .appending("appid", value: appikey)
            .appending("units", value: "metric")
            .appending("lang", value: "es")
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = "GET"
        
        // Define URL for Weather Data
        URLSession.shared.dataTask(with: finalURL) { data, response, error in
            guard error == nil else {
                return completionHandler(.error(NetworkResponse.failed.rawValue))
            }
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        return completionHandler(.error(NetworkResponse.noData.rawValue))
                    }
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        decoder.dateDecodingStrategy = .secondsSince1970
                        let result = try decoder.decode(CityWeather.self, from: responseData)
                        completionHandler(.success(result))
                    } catch {
                        completionHandler(.error(NetworkResponse.unableToDecode.rawValue))
                    }
                    
                case .error(let networkFailureError):
                    completionHandler(.error(networkFailureError))
                }
            }
       }.resume()
    }
    
    func fetchExtendedWeather(lat: Double, lon: Double, completionHandler: @escaping ApiCompletionHandler<[Daily]>) {
        let url = baseURL.appendingPathComponent("onecall")
        let finalURL = url.appending("lat", value: "\(lat)")
            .appending("lon", value: "\(lon)")
            .appending("exclude", value: "current,minutely,hourly,alerts")
            .appending("appid", value: appikey)
            .appending("units", value: "metric")
            .appending("lang", value: "es")
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = "GET"
        
        // Define URL for Weather Data
        URLSession.shared.dataTask(with: finalURL) { data, response, error in
            guard error == nil else {
                return completionHandler(.error(NetworkResponse.failed.rawValue))
            }
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        return completionHandler(.error(NetworkResponse.noData.rawValue))
                    }
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        decoder.dateDecodingStrategy = .secondsSince1970
                        let result = try decoder.decode(CityExtendedWeather.self, from: responseData)
                        completionHandler(.success(result.daily))
                    } catch {
                        completionHandler(.error(NetworkResponse.unableToDecode.rawValue))
                    }
                    
                case .error(let networkFailureError):
                    completionHandler(.error(networkFailureError))
                }
            }
       }.resume()
    }
    
    //MARK: - PRIVATE METHOD
    
    private func handleNetworkResponse(_ response: HTTPURLResponse) -> ApiResult<String> {
        switch response.statusCode {
        case 200...299:
            return .success("")
        case 401...500:
            return .error(NetworkResponse.authenticationError.rawValue)
        case 501...599:
            return .error(NetworkResponse.badRequest.rawValue)
        case 600:
            return .error(NetworkResponse.outdated.rawValue)
        default:
            return .error(NetworkResponse.failed.rawValue)
        }
    }

}
