//
//  ListViewModel.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 14/01/2021.
//

import Foundation
import UIKit

class ListViewModel {
    let cityManager = CityManager.shared

    //MARK: PUBLIC METHOD
    func fetch() {
        cityManager.fetch()
    }
    
    func getCityWeatherInfo(index: IndexPath) -> CityWeather? {
        switch TableViewSection(rawValue: index.section) {
        case .currentLocation:
            return cityManager.currentLocation
        case .bookmark:
            return cityManager.weatherList[index.row]
        default:
            return nil
        }
    }
}

// MARK:  UITableViewDataSource
extension ListViewModel {
    func getNumberOfRows() -> Int {
        return cityManager.weatherList.count
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        switch TableViewSection(rawValue: section) {
        case .currentLocation:
            return cityManager.currentLocation != nil ? 1 : 0
        case .bookmark:
            return cityManager.weatherList.count
        case .none:
            return 0
        }
    }
    
    func getTitleForSection(section: Int) -> String {
        switch TableViewSection(rawValue: section) {
        case .currentLocation:
            return cityManager.currentLocation != nil ? "Current Location" : ""
        case .bookmark:
            return  cityManager.weatherList.count > 0 ? "Save Bookmarks" : ""
        case .none:
            return ""
        }
    }
    
    
    func getCell(fromTableView tableView: UITableView, atIndexPath indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeatherTableViewCell.identifier()) as? WeatherTableViewCell else {
            fatalError("Cell not registered on tableView")
        }
        if indexPath.section == TableViewSection.currentLocation.rawValue {
            if let currentLocation = cityManager.currentLocation {
                cell.viewModel = WeatherCellViewModel(withCity: currentLocation, isCurrentLocation: true)
            }
           
        } else {
            cell.viewModel = WeatherCellViewModel(withCity: cityManager.weatherList[indexPath.row], isCurrentLocation: false)
        }
        
        cell.viewModel?.updateTableView = { [weak self] (city) in
            guard let strongSelf = self else { return }
            Thread.runOnMainQueue {
                strongSelf.cityManager.removeCity(city: city)
            }
        }
        return cell
    }
}
