//
//  SearchViewModel.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 15/01/2021.
//

import Foundation
import UIKit

class SearchViewModel {
    
    let cityManager = CityManager.shared
    
    lazy var apiClient: WeatherAPIProtocol = {
        return WeatherAPIManager()
    }()
    
    public var searchResult: [CityWeather] = [] {
        didSet {
            searchListBinding?()
        }
    }
    
    var searchListBinding: (() -> Void)?
    var errorHandler: ((String) -> Void)?
    
    //MARK: PUBLIC METHOD
    func findBy(textValue: String) {
        let completionHandler: ApiCompletionHandler<SearchResult> = { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let searchResult):
                strongSelf.searchResult = searchResult.list
            case .error(let error):
                strongSelf.errorHandler?(error)
            }
        }
        apiClient.findBy(cityName: textValue, completionHandler: completionHandler)
    }
    
    func cleanList() {
        searchResult = []
    }
}

// MARK:  UITableViewDataSource
extension SearchViewModel {
    func getNumberOfRows() -> Int {
        return searchResult.count
    }
    
    func getCell(fromTableView tableView: UITableView, atIndexPath indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeatherTableViewCell.identifier()) as? WeatherTableViewCell else {
            fatalError("Cell not registered on tableView")
        }
        
        cell.viewModel = WeatherCellViewModel(withCity: searchResult[indexPath.row], isCurrentLocation: false)
        cell.viewModel?.updateTableView = { [weak self] (city) in
            guard let strongSelf = self else { return }
            strongSelf.cityManager.addNewCity(city: city)
        }
        
        return cell
    }
}

