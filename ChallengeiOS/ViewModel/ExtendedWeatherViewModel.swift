//
//  ExtendedWeatherViewModel.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 16/01/2021.
//

import Foundation
import UIKit

class ExtendedWeatherViewModel {
    
    var cityWeather: CityWeather
    
    lazy var apiClient: WeatherAPIProtocol = {
        return WeatherAPIManager()
    }()
    
    public var extendedResult: [Daily] = [] {
        didSet {
            extendedListBinding?()
        }
    }
    
    var extendedListBinding: (() -> Void)?
    var errorHandler: ((String) -> Void)?
    
    init(cityWeather: CityWeather) {
        self.cityWeather = cityWeather
    }
    
    //MARK: PUBLIC METHOD
    func fetch() {
        let completionHandler: ApiCompletionHandler<[Daily]> = { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let extendedResult):
                strongSelf.extendedResult = extendedResult
            case .error(let error):
                strongSelf.errorHandler?(error)
            }
        }
        apiClient.fetchExtendedWeather(lat: cityWeather.coord.lat, lon: cityWeather.coord.lon, completionHandler: completionHandler)
    }
    
    func getDaily(atIndex index: Int) -> Daily {
        return extendedResult[index]
    }
}
// MARK: - UITableViewDataSource
extension ExtendedWeatherViewModel {
    func getNumberOfRows() -> Int {
        return extendedResult.count 
    }
    
    func getCell(fromTableView tableView: UITableView, atIndexPath indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ExtendedWeatherTableViewCell.identifier()) as? ExtendedWeatherTableViewCell else {
            fatalError("cell not registered on tableView")
        }
        
        cell.viewModel = ExtendedWeatherCellViewModel(withDailyInfo: extendedResult[indexPath.row])
        return cell
    }
}
