//
//  WeatherCellViewModel.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 14/01/2021.
//

import Foundation

class WeatherCellViewModel {
    
    private var city: CityWeather
    
    var isCurrentLocation: Bool
    
    var updateTableView: ((_ currentWeather: CityWeather) -> Void)?
    
    init(withCity city: CityWeather, isCurrentLocation: Bool) {
        self.city = city
        self.isCurrentLocation = isCurrentLocation
    }
    
    // MARK: - PUBLIC METHOD
    
    func getCityName() -> String {
        return city.name
    }
    
    func getIconUrl() -> String {
        guard let currentWeather = city.weather.first else { return "" }
        return "https://openweathermap.org/img/w/\(currentWeather.icon).png"
    }
    
    func getFlagUrl() -> String {
        return "https://openweathermap.org/images/flags/\(city.sys.country.lowercased()).png"
    }
    
    func getDate() -> String {
        String.getDateFromTimeStamp(timeStamp: city.dt, dateFormat: "dd MMM YY, hh:mm a")
    }
    
    func getDescription() -> String {
        guard let currentWeather = city.weather.first else { return "" }
        return currentWeather.description
    }
    
    func getTemp() -> String {
        return "\(city.main.temp.rounded())°C"
    }
    
    func isInWeatherList() -> Bool {
        return CityManager.shared.weatherList.contains(city)
    }
    
    func didTapBookmarkButton() {
        updateTableView?(city)
    }
    
}


