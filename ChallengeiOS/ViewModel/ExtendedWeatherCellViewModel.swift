//
//  ExtendedWeatherCellViewModel.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 16/01/2021.
//

import Foundation

class ExtendedWeatherCellViewModel {
    private var dailyWeather: Daily
    
    init(withDailyInfo daily: Daily) {
        self.dailyWeather = daily
    }
    
    //MARK: PUBLIC METHOD
    func getDate() -> String{
        return String.getDateFromTimeStamp(timeStamp: dailyWeather.dt, dateFormat: "E, MMM d")
    }
    
    func getIconUrl() -> String {
        guard let currentWeather = dailyWeather.weather.first else { return "" }
        return "https://openweathermap.org/img/w/\(currentWeather.icon).png"
    }
    
    func getTemp() -> String {
        return "\(dailyWeather.temp.min.rounded())/\(dailyWeather.temp.max.rounded())°C"
    }
}
