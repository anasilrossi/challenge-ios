//
//  CityManager.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 16/01/2021.
//

import Foundation

private let _shared:CityManager = CityManager()

class CityManager {
    
    // MARK: - Initialization
    public class var shared:CityManager {
        return _shared
    }
    
    public var weatherList: [CityWeather] = [] {
        didSet {
            weatherListBinding?()
            saveInUserDefault()
        }
    }
    
    public var currentLocation: CityWeather? {
        didSet {
            weatherListBinding?()
        }
    }
    
    private lazy var apiClient: WeatherAPIProtocol = {
        return WeatherAPIManager()
    }()
    
    private lazy var locationManager: LocationManager = {
       return LocationManager()
    }()
    
    var weatherListBinding: (() -> Void)?
    var errorHandler: ((String) -> Void)?

    //MARK: - PUBLIC METHOD
    func fetch() {
        locationManager.currentLocationBinding = { [weak self] (lat, long) in
            guard let strongSelf = self else { return }
            strongSelf.getCurrentWeather(lat: lat, long: long)
        }
        locationManager.errorHandler = errorHandler
        locationManager.obtainLocation()
        
        if let cityIds = obtainInfoFromUserDefault(), cityIds.count > 0 {
            fetch(cityID: cityIds)
        }
    }
    
    func addNewCity(city: CityWeather) {
        if !weatherList.contains(city) {
            weatherList.append(city)
        }
    }
    
    func removeCity(city: CityWeather) {
        if let index = weatherList.firstIndex(of: city) {
            weatherList.remove(at: index)
        }
    }
    
    //MARK: - PRIVATE METHOD
    private func getCurrentWeather(lat: Double, long: Double) {
        let completionHandler: ApiCompletionHandler<CityWeather> = { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let currentWeather):
                strongSelf.currentLocation = currentWeather
            case .error(let error):
                strongSelf.errorHandler?(error)
            }
        }
        apiClient.fetchBy(lat: lat, long: long, completionHandler: completionHandler)
    }
    
    private func fetch(cityID: [Int]) {
        let completionHandler: ApiCompletionHandler<SearchResult> = { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let cityList):
                strongSelf.weatherList = cityList.list
            case .error(let error):
                strongSelf.errorHandler?(error)
            }
        }
        apiClient.fetchBy(cityID: cityID, completionHandler: completionHandler)
    }
    
    private func obtainCitysId() -> [Int] {
        return weatherList.map({$0.id})
    }
    
    private func saveInUserDefault() {
        let cityIDs = obtainCitysId()
        let defaults = UserDefaults.standard
        defaults.set(cityIDs, forKey: .cityIdsArray)
    }
    
    private func obtainInfoFromUserDefault() -> [Int]? {
        let defaults = UserDefaults.standard
        return defaults.array(forKey: .cityIdsArray) as? [Int]
    }
}

fileprivate extension String {
    static let cityIdsArray = "cityIdsArray"
}
