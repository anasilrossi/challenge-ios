//
//  LocationManager.swift
//  ChallengeiOS
//
//  Created by Analia Rossi on 15/01/2021.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    var locationManager = CLLocationManager()
    
    public var currentLocation: CLLocationCoordinate2D = CLLocationCoordinate2D() {
        didSet {
            currentLocationBinding?(currentLocation.latitude, currentLocation.longitude)
        }
    }
    
    var currentLocationBinding: ((_ latitude: Double, _ longitud: Double) -> Void)?
    var errorHandler: ((String) -> Void)?
    
    func obtainLocation() {
        /// Asking for UseAuthorization
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            /// Obtein user location
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            guard let location: CLLocationCoordinate2D = locationManager.location?.coordinate else {
                errorHandler?("Please authorize the location to better the experience")
                return }
            currentLocation = location
        } 
    }
}
